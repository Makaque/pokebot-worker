// python-build-start
// arduino:avr:mega:cpu=atmega2560
// /dev/ttyACM0
// python-build-end

#include <Servo.h>
#include <button.h>

int led = 13;
int msgStart = 255; // All bits on
int servoPortA = 2;
int servoPortB = 3;
int servoPortX = 4;
int servoPortY = 5;
int servoPortUp = 6;
int servoPortDwn = 7;
int servoPortLft = 8;
int servoPortRgt = 9;
Servo servoA;
Servo servoB;
Servo servoX;
Servo servoY;
Servo servoUp;
Servo servoDwn;
Servo servoLft;
Servo servoRgt;
int on = HIGH;
int off = LOW;
int pressedVal = 1200;
int releasedVal = 80;


int numBtns = 8;

void pulse(int pulses, int length = 500){
  for(int i = 0; i < pulses; i++){
    if(i) delay(length);
    digitalWrite(led, on);
    delay(length);
    digitalWrite(led, off);
  }
}

bool pressed(Button btn, int input){
  return (input >> (btn - 1)) & 1;
}

// Debugging behaviour
void mimicD(Button btn, bool isPressed){
  delay(200);
  if(isPressed){
    pulse(1, 500);
  } else {
    pulse(1, 100);
  }
    delay(200);
}

void mimic(Button btn, bool isPressed){
  int pushVal = isPressed? pressedVal : releasedVal;
  switch(btn){
    case A:
      servoA.write(pushVal);
      break;
    case B:
      servoB.write(pushVal);
      break;
    case X:
      servoX.write(pushVal);
      break;
    case Y:
      servoY.write(pushVal);
      break;
    case UP:
      servoUp.write(pushVal);
      break;
    case DWN:
      servoDwn.write(pushVal);
      break;
    case LFT:
      servoLft.write(pushVal);
      break;
    case RGT:
      servoRgt.write(pushVal);
      break;
    default:
      break;
  }
}

void btnInc(Button &b){
  b = (Button)((int)b + 1);
}

void decode(int input){
  for(Button i = (Button)1; i <= numBtns; btnInc(i)){
    mimic(i, pressed(i, input));
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  servoA.attach(servoPortA);
  servoB.attach(servoPortB);
  servoX.attach(servoPortX);
  servoY.attach(servoPortY);
  servoUp.attach(servoPortUp);
  servoDwn.attach(servoPortDwn);
  servoLft.attach(servoPortLft);
  servoRgt.attach(servoPortRgt);
  while(!Serial){/*Wait*/}
}

void loop() {
   // put your main code here, to run repeatedly:
  // digitalWrite(led, off);
  while(Serial.available() && Serial.read() == msgStart){
    while(!Serial.available()){}
    int specialMode = Serial.read();
    while(!Serial.available()){}
    int controllerState = Serial.read();
    decode(controllerState);
  }
}

